#include <linux/soundcard.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <time.h>

int doMidi();
void delay();

// Sloppy
int main(void) {
   int fd = open("/dev/midi1", O_WRONLY, 0);

    fork();
    doMidi(fd, "/dev/midi1", 0x80, 0x90, 60);

    fork();
    doMidi(fd, "/dev/midi1", 0x80, 0x90, 70);

    fork();
    doMidi(fd, "/dev/midi1", 0x80, 0x90, 80);

   close(fd);

   return 0;
}

int doMidi(int fd, char* device, int upCode, int downCode, int note) {
   unsigned char upData[3] = {upCode, note, 127};
   unsigned char data[3] = {downCode, note, 127};

   // step 1: open the OSS device for writing
   if (fd < 0) {
      printf("Error: cannot open %s\n", device);
      return 1;
   }

   // step 2: write the MIDI information to the OSS device
   write(fd, data, sizeof(data));

   delay(700);

   write(fd, upData, sizeof(upData));


   // step 3: (optional) close the OSS device

   return 0;
}

void delay(int t) {
    int start = clock();

    while(clock() < start + (t * 1000)) {
    }
}
